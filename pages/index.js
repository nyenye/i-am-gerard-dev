import React from "react";
import {
  Layout,
  Sider,
  Main,
  Welcome,
  WhoAmI,
  MyCareer,
  Footer
} from "../components";

const Home = () => (
  <Layout>
    <Sider></Sider>
    <Main>
      <Welcome />
      <WhoAmI />
      <MyCareer />
      <Footer />
    </Main>
  </Layout>
);

export default Home;
