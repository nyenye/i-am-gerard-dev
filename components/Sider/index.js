import React from "react";
import PropTypes from "prop-types";
import { Home, Person, Work } from "@material-ui/icons";

import Link from "next/link";

import styles from "./index.module.scss";

const links = [
  {
    title: "Welcome",
    url: "#welcome",
    icon: <Home />
  },
  {
    title: "Who Am I",
    url: "#who-am-i",
    icon: <Person />
  },
  {
    title: "My career",
    url: "#my-career",
    icon: <Work />
  }
];

function SiderLink({ title, url, icon, onClick, active = false }) {
  return (
    <li
      className={styles.anchor}
      data-active={active}
      data-hash={url.substring(1)}
    >
      <Link href={url}>
        <a onClick={onClick}>
          {icon} <span>{title}</span>
        </a>
      </Link>
    </li>
  );
}

SiderLink.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool
};

function Sider() {
  const [activeLink, setActiveLink] = React.useState("");
  const [isHidden, setIsHidden] = React.useState(false);

  const lastScrollY = React.useRef(null);
  const lastDeltaScrollY = React.useRef(0);

  function getActiveLink() {
    const target = window.innerHeight / 3;

    const welcomeSection = document
      .getElementById("welcome")
      .getBoundingClientRect();
    const myCareerSection = document
      .getElementById("my-career")
      .getBoundingClientRect();
    const whoAmISection = document
      .getElementById("who-am-i")
      .getBoundingClientRect();

    const deltaWelcome = Math.abs(
      target - (welcomeSection.top + welcomeSection.height / 3)
    );
    const deltaWhoAmI = Math.abs(
      target - (whoAmISection.top + whoAmISection.height / 2)
    );
    const deltaMyCareer = Math.abs(
      target - (myCareerSection.top + myCareerSection.height / 3)
    );

    if (deltaWelcome < deltaWhoAmI && deltaWelcome < deltaMyCareer)
      return "#welcome";

    if (deltaWhoAmI < deltaMyCareer) return "#who-am-i";

    return "#my-career";
  }

  function hasChangedScrollDirection(lastDelta, currDelta) {
    if (lastDelta < 0 && lastDelta - currDelta < 0) return true;

    if (lastDelta > 0 && lastDelta - currDelta > 0) return true;

    return false;
  }

  function shouldHide() {
    const currentScrollY = document.documentElement.scrollTop;
    if (lastScrollY.current === null) lastScrollY.current = currentScrollY;

    const deltaScrollY = lastScrollY.current - currentScrollY;
    const hasChangedDirection = hasChangedScrollDirection(
      lastDeltaScrollY.current,
      deltaScrollY
    );

    lastDeltaScrollY.current = deltaScrollY;
    if (hasChangedDirection) lastScrollY.current = currentScrollY;

    if (lastScrollY.current - currentScrollY < 0) return true;

    return false;
  }

  function onScroll() {
    const active = getActiveLink();
    setActiveLink(active);

    const isHidden = shouldHide();
    setIsHidden(isHidden);
  }

  React.useEffect(() => {
    const active = getActiveLink();
    setActiveLink(active);
  }, [setActiveLink]);

  React.useEffect(() => {
    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  });

  function onSiderLinkClick(e) {
    e.preventDefault();

    const link = e.target.parentElement;
    const section = document
      .getElementById(link.dataset.hash)
      .getBoundingClientRect();

    const targetScrollY = document.documentElement.scrollTop + section.top;

    document.documentElement.scrollTo({
      left: 0,
      top: targetScrollY,
      behavior: "smooth"
    });
  }

  return (
    <nav className={styles.container} data-hidden={isHidden}>
      <ul className={styles.menu}>
        {links.map(link => {
          return (
            <SiderLink
              key={link.url}
              {...link}
              onClick={onSiderLinkClick}
              active={activeLink === link.url}
            />
          );
        })}
      </ul>
    </nav>
  );
}

export { Sider };
