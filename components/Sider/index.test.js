import React from "react";
import { render } from "@testing-library/react";

import { Sider } from "./index";

test("Sider - it renders", () => {
  const { getByText } = render(<Sider></Sider>);

  expect(getByText("Welcome")).toHaveTextContent("Welcome");
  expect(getByText("Who Am I")).toHaveTextContent("Who Am I");
  expect(getByText("What I Do")).toHaveTextContent("What I Do");
});
