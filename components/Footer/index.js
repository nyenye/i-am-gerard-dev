import React from "react";
import { Favorite } from "@material-ui/icons";

import styles from "./index.module.scss";

function Footer() {
  return (
    <footer className={styles.container}>
      <p>Made with</p>
      <Favorite />
      <div className={styles.separator} />
      <a href="https://gitlab.com/nyenye/i-am-gerard-dev">Source</a>
    </footer>
  );
}

export { Footer };
