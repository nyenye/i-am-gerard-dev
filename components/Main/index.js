import React from "react";
import PropTypes from "prop-types";

import styles from "./index.module.scss";

function Main({ children }) {
  return <main className={styles.container}>{children}</main>;
}

Main.propTypes = {
  children: PropTypes.node.isRequired
};

export { Main };
