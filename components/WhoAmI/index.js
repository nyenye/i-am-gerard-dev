import React from "react";
import { Email, Phone, GitHub, LinkedIn } from "@material-ui/icons";

import { CopyToClipboard } from "react-copy-to-clipboard";
import styles from "./index.module.scss";

function WhoAmI() {
  const intervalRef = React.useRef(null);
  const [justCopied, setJustCopied] = React.useState(null);

  function onCopy(copiedTarget) {
    setJustCopied(copiedTarget);

    if (intervalRef.current !== null) clearTimeout(intervalRef.current);
    const id = setTimeout(() => {
      setJustCopied(null);
      intervalRef.current = null;
    }, 800);

    intervalRef.current = id;
  }

  return (
    <section id="who-am-i" className={styles.container}>
      <div>
        <img
          className={styles.profilePic}
          src="/images/undraw_male_avatar.svg"
        />
      </div>

      <div className={styles.description}>
        <p>
          I&apos;m Gerard, a developer who aspires to be proud of his work
          anywhere he goes. One of my favorite aspects of my job, is to make a
          reality what clients ask me to.
        </p>
        <p>
          My strengths lie within the JavaScript ecosystem; from frontend
          frameworks like React or Vue, to backend tools like Node.js and ORMs.
          Also have a strong affinity with the development of responsive
          designs, or mobile apps with React Native or Flutter.
        </p>
        <p>
          On another note, I&apos;m really into game development using open
          source tools like Godot Engine, Blender and Inkscape.
        </p>
      </div>

      <div className={styles.info}>
        <p>
          <CopyToClipboard
            text="gerard.purra@gmail.com"
            onCopy={() => onCopy("email")}
          >
            <i>
              <Email /> <b>gerard.purra@gmail.com</b>
            </i>
          </CopyToClipboard>
          {justCopied === "email" && <span>Copied email!</span>}
        </p>
        <p>
          <CopyToClipboard text="+34 658 834 907" onCopy={() => onCopy("telf")}>
            <i>
              <Phone /> <b>+34 658 834 907</b>
            </i>
          </CopyToClipboard>
          {justCopied === "telf" && <span>Copied phone!</span>}
        </p>
        <p>
          <a
            href="https://www.linkedin.com/in/gerard-purra-teixido/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <LinkedIn /> <b>https://linkedin.com</b>
          </a>
        </p>
        <p>
          <a
            href="https://github.com/nyenye/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <GitHub /> <b>https://github.com</b>
          </a>
        </p>
      </div>
    </section>
  );
}

export { WhoAmI };
