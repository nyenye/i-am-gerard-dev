import React from "react";
import PropTypes from "prop-types";
import { Storage, PhoneAndroid, DesktopMac } from "@material-ui/icons";

// import Link from "next/link";

import styles from "./index.module.scss";

const articles = [
  {
    title: "Front-end & Web Apps",
    description: (
      <div>
        <p>
          I started my carrer as a front-end developer, working at a company as
          an intern, helping to improve the company&apos;s administration site
          and it&apos;s homepage, and building the major part of a website for a
          client.
        </p>
        <p>
          From there, I went on to learn CMS like Wordpress by taking on
          freelance work ranging from upgrading a site to the latest CMS version
          and changing it&apos;s theme and adapting it to the client&apos;s
          needs with Prestashop, remaking a legacy custom e-shop using
          Woocommerce by migrating their current products, or building a site
          and it&apos;s theme from scratch with Wordpress.
        </p>
        <p>
          I&apos;ve since then learned modern frontend framework libraries like
          React, and have keep up to date with it&apos;s newest additions like
          context and hooks. This website is powered by Next.js, a static site
          generator built on top of React.
        </p>
      </div>
    ),
    image: {
      src: "/images/undraw_web_developer_p3e5.svg",
      alt: "Building front end web apps"
    },
    icon: <DesktopMac />
  },
  {
    title: "Back-end Developer",
    description: (
      <div>
        <p>
          As for server side, I&apos;m well versed in Node.js with Express as
          main framework, having built the backend for two projects; a medical
          application to manage complications which served a RestAPI, and a
          banking simulator which made use of GraphQL.
        </p>
        <p>
          To keep a clean development environment the use of containers has
          become a standard, solving at the same the “it works on my machine”
          headache, and having an environment as close to the production one as
          possible.
        </p>
        <p>
          Data, plays a major role on any kind of application, and Sequelize has
          been my go to when talking about ORMs, given it&apos;s active
          community, and lengthy documentation.
        </p>
        <p>
          Finally, in recent projects I&apos;ve come to use TypeScript, given
          that it&apos;s starting to take up momentum, and it has been adopted
          by all major frameworks, be it frontend libraries like React, or ORMs
          like Sequelize.
        </p>
      </div>
    ),
    image: {
      src: "/images/undraw_code_review_l1q9.svg",
      alt: "Writting backend code"
    },
    icon: <Storage />
  },
  {
    title: "Mobile Experiences",
    description: (
      <div>
        <p>
          By taking advantage of modern technologies, in particular React Native
          and Flutter, I&apos;ve tapped into the world of mobile app
          development.
        </p>
        <p>
          React Native, was the fraemwork used to create the Android and iOS
          application &ldquo;Complication Management&rdquo;, a propietary app
          used to manage medical issues among health care providers.
        </p>
        <p>
          With Flutter, I&apos;ve build a &ldquo;Hackernews&rdquo; clone app,
          open source, which can be found at my{" "}
          <a href="gitlab.com/nyenye/flutternews">GitLab</a> profile.
        </p>
      </div>
    ),
    image: {
      src: "/images/undraw_mobile_development_8gyo-flipped.svg",
      alt: "Developing a mobile app"
    },
    icon: <PhoneAndroid />
  }
];

function Article({ title, description, icon, image, index }) {
  return (
    <article
      style={{ "--lighten-index": `${index * 15}%` }}
      className={styles.articleWrapper}
    >
      <div className={styles.article}>
        <h3>
          {icon} {title}
        </h3>
        {description}
        <img src={image.src} alt={image.alt}></img>
      </div>
    </article>
  );
}

Article.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.node.isRequired,
  icon: PropTypes.node.isRequired,
  image: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired
};

function MyCareer() {
  return (
    <section id="my-career" className={styles.container}>
      {articles.map((item, index) => (
        <Article key={item.title} {...item} index={index} />
      ))}
    </section>
  );
}

export { MyCareer };
