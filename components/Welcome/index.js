import React from "react";

import styles from "./index.module.scss";

function Welcome() {
  return (
    <section id="welcome" className={styles.container}>
      <h1>
        Crafting <span>customer tailored</span> experiences
      </h1>
      <h3>Full-Stack JavaScript Developer from Barcelona</h3>
      <img src="/images/undraw_deliveries_131a.svg" />
    </section>
  );
}

export { Welcome };
